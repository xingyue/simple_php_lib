<?php

define('LIB_ROOT',dirname(dirname(__FILE__)));

spl_autoload_register('lib_auto_load');

function lib_auto_load($class_name){
	$class_dir = preg_replace('/_/','/',$class_name);
	$class_file = LIB_ROOT . '/' . $class_dir . '.php';
	if(file_exists($class_file)){
		include $class_file ;	
		return true;
	}
}
