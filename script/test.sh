#!/bin/sh

function green(){
	echo "\033[32;40m$1\033[0m"
}

U_PHP=/usr/bin/php

for f in $(find ../test -name "*.php")
do
	echo "============================================"
	green $f
	$U_PHP $f
	echo 
done



